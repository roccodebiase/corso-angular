package it.nolme.tutorial.angular6backend.dao;

import it.nolme.tutorial.angular6backend.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface UserDao extends CrudRepository<User,Long> {
	List<User> findAll();

	User findByName(String name);

	@Query("SELECT u FROM User u where u.id = :id")
	User findUserById(@Param("id") Long id);

	User save(User user);

	void deleteById(Long id);
}
