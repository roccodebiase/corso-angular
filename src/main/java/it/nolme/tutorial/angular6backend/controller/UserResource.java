package it.nolme.tutorial.angular6backend.controller;

import it.nolme.tutorial.angular6backend.model.User;
import it.nolme.tutorial.angular6backend.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600,	allowedHeaders={"x-auth-token", "x-requested-with", "x-xsrf-token"})
@RestController
@RequestMapping("/rest/user")
public class UserResource {

	private static final Logger logger = Logger.getLogger(UserResource.class);
	@Autowired
	private UserService userService;

	@RequestMapping("/users")
	public List<User> findAllUsers() {
		logger.debug("---------- request to server request:{}");
		return userService.findAllUsers();
	}

	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public User getUserById(@PathVariable Long userId) {
		return userService.findById(userId);
	}

	@RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
	public void deleteUser(@PathVariable Long userId) {
		userService.delete(userId);
	}


	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public User updateUser(@RequestBody User user) {
		return userService.save(user);
	}
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public User saveUser(@RequestBody User user) {
		user.setUsername("test_"+Math.subtractExact(1,10000));
		user.setPassword(Math.subtractExact(1,20000)+"");
		return userService.save(user);
	}
}
