package it.nolme.tutorial.angular6backend.service.impl;

import it.nolme.tutorial.angular6backend.dao.UserDao;
import it.nolme.tutorial.angular6backend.model.User;
import it.nolme.tutorial.angular6backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	@Override
	public List<User> findAllUsers() {
		
		return userDao.findAll();
	}

	@Override
	public User findById(Long id) {
		return userDao.findUserById(id);
	}

	@Override
	public User findByUserName(String name) {
		return userDao.findByName(name);
	}

	@Override
	public User save(User user) {
		return userDao.save(user);
	}

	@Override
	public void delete(Long userId) {
		userDao.deleteById(userId);
	}

}
