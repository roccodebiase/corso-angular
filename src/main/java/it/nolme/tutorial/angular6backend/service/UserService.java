package it.nolme.tutorial.angular6backend.service;

import it.nolme.tutorial.angular6backend.model.User;

import java.util.List;

public interface UserService {
	List<User> findAllUsers();
	User findById(Long id);
	User findByUserName(String userName);
	
	User save(User user);

	void delete(Long userId);
}
